<?php
require __DIR__ . '/vendor/autoload.php';

use League\Csv\Reader;

$reader = Reader::createFromPath('catalog.csv', 'r');
$reader->setDelimiter(';');

$records = $reader->getRecords();
$header = [];

$xml = new DOMDocument('1.0', 'utf-8');
$xml->formatOutput = true;
$root = add_xml_elem('catalog', null, $xml);

foreach ($records as $key => $row) {
    if ($key) {
        $offer = add_xml_elem('offer', null, $root);
        add_attr('id', $row[0], $offer);
        foreach ($row as $index => $value) {
            if($value && $index > 18){
                $prop = add_xml_elem('property', $value, $offer);
                add_attr('name', $header[$index], $prop);
            }
        }
        // break;
        save_xml();
    } else {
        $header = $row;
    }
}



function save_xml(string $filename = 'xml.xml')
{
    global $xml;
    $xml->save($filename);
}

function add_xml_elem(string $node_name, ?string $value, DOMNode $parent_node)
{
    global $xml;
    $value = htmlspecialchars($value);
    $elem = $xml->createElement($node_name, $value);
    $parent_node->appendChild($elem);
    return $elem;
}

function add_attr(string $attr_name, string $value, DOMNode $node)
{
    global $xml;
    $attr = $xml->createAttribute($attr_name);
    $attr->value = $value;
    $node->appendChild($attr);
}
