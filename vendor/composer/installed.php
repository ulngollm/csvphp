<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'league/csv' => array(
            'pretty_version' => '9.7.2',
            'version' => '9.7.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/csv',
            'aliases' => array(),
            'reference' => '8544655c460fd01eed0ad258e514488d4b388645',
            'dev_requirement' => false,
        ),
    ),
);
